﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour
{

    private Transform playerTransform;
    private Vector3 newPos;

    public Transform ballPosition;

    public float speed;
    private float move;

    void Start()
    {
        // Equivalente a playerTransform = GetComponent<Transform> ();
        playerTransform = transform;

        newPos = playerTransform.position;
    }

    void Update()
    {
        if (ballPosition.position.x > transform.position.x)
        {
            move = 1;
        }
        else
        {
            move = -1;
        }
        move *= Time.deltaTime;
        move *= speed;
        transform.Translate(0, move, 0);

        if (transform.position.x < -10.0f)
        {
            transform.position = new Vector3(-10.0f, transform.position.y, transform.position.z);
        }
        else if (transform.position.x > 8.0f)
        {
            transform.position = new Vector3(8.0f, transform.position.y, transform.position.z);
        }
    }
}
