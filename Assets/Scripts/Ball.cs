﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	public Vector2 speed;

	private Vector2 iniSpeed;
	private Vector3 iniPos;

	// Use this for initialization
	void Start () 
	{
		iniSpeed = speed;
		iniPos = transform.position;
	}

	// Update is called once per frame
	void Update () 
	{
		transform.Translate (speed.x*Time.deltaTime, speed.y*Time.deltaTime, 0);
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Bounds") 
		{
			speed.y *= -1.15f;
		}
		if (other.tag == "Player") 
		{
			speed.x *= -1.20f;
		}
        if (other.tag == "Enemy")
        {
            speed.y *= -1.20f;
        }
        else if (other.tag == "Goal") 
		{
			Reset ();
		}

        if (speed.x > 17f)
        {
            speed.x = 17f;
        }
        else if (speed.x < -17f)
        {
            speed.x = -17f;
        }

        if (speed.y > 17f)
        {
            speed.y = 17f;
        }
        else if (speed.y < -17f)
        {
            speed.y = -17f;
        }
    }

	void Reset()
	{
		transform.position = iniPos;
		speed = iniSpeed;
	}
}
